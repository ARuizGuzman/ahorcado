class Ahorcado{

    constructor(){
        this.inicio();
    }

    inicio() {

        let lienzo= document.getElementById('lienzo');
        lienzo.width = window.innerWidth;
        lienzo.height = window.innerHeight;
        ctx = lienzo.getContext('2d');
        ctx.stroke();
        ctx.translate(0, window.innerHeight);
        ctx.scale(1, -1);
     
    }

    Tecla(x, y, ancho, alto, letra){
        this.x = x;
        this.y = y;
        this.ancho = ancho;
        this.alto = alto;
        this.letra = letra;
        this.dibuja = dibujaTecla;
    }
    
    Letra(x, y, ancho, alto, letra){
        this.x = x;
        this.y = y;
        this.ancho = ancho;
        this.alto = alto;
        this.letra = letra;
        this.dibuja = dibujaCajaLetra;
        this.dibujaLetra = dibujaLetraLetra;
    }

    dibujaTecla(){
        ctx.fillStyle = colorTecla;
        ctx.strokeStyle = colorMargen;
        ctx.fillRect(this.x, this.y, this.ancho, this.alto);
        ctx.strokeRect(this.x, this.y, this.ancho, this.alto);
        
        ctx.fillStyle = "white";
        ctx.font = "bold 20px courier";
        ctx.fillText(this.letra, this.x+this.ancho/2-5, this.y+this.alto/2+5);
    }

    teclado(){
        var ren = 0;
        var col = 0;
        var letra = "";
        var miLetra;
        var x = inicioX;
        var y = inicioY;
        for(var i = 0; i < letras.length; i++){
            letra = letras.substr(i,1);
            miLetra = new Tecla(x, y, lon, lon, letra);
            miLetra.dibuja();
            teclas_array.push(miLetra);
            x += lon + margen;
            col++;
            if(col==10){
                col = 0;
                ren++;
                if(ren==2){
                    x = 280;
                } else {
                    x = inicioX;
                }
            }
            y = inicioY + ren * 50;
        }
    }
}



